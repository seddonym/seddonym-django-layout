from .project import ProjectConfiguration
from configurations_seddonym import installations
import os


class Local(installations.LocalMixin, ProjectConfiguration):
    PROJECT_ROOT = '/home/david/www/{{ project_name }}'
    EMAIL_HOST_USER = ''
    EMAIL_HOST = ''
    SERVER_EMAIL = ''
    ACCOUNT_PASSWORD_MIN_LENGTH = 1


class Dev(installations.WebfactionDevMixin, ProjectConfiguration):
    DOMAIN = ''
    WEBFACTION_USER = ''
    EMAIL_HOST_USER = ''

    ACCOUNT_PASSWORD_MIN_LENGTH = 1


class Live(installations.WebfactionLiveMixin, ProjectConfiguration):
    DOMAIN = ''
    WEBFACTION_USER = ''
    EMAIL_HOST_USER = ''

    ACCOUNT_PASSWORD_MIN_LENGTH = 6


    AWS_ACCESS_KEY_ID = ''
    AWS_BUCKET_NAME = '{{ project_name }}-backups-media'

    DBBACKUP_STORAGE = 'dbbackup.storage.s3_storage'
    DBBACKUP_S3_BUCKET = '{{ project_name }}-backups-db'

    @property
    def DBBACKUP_S3_ACCESS_KEY(self):
        return self.AWS_ACCESS_KEY_ID

    @property
    def DBBACKUP_S3_SECRET_KEY(self):
        return self.AWS_SECRET_ACCESS_KEY
