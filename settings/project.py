from configurations_seddonym import StandardConfiguration
import os


class ProjectConfiguration(StandardConfiguration):
    SITE_TITLE = ''
    PROJECT_NAME = '{{ project_name }}'
    INSTALLED_APPS = StandardConfiguration.INSTALLED_APPS + (
        # Apps lower down the list should import from apps higher up the list,
        # and not the other way around
        'django.contrib.humanize',
        'django.contrib.gis',
        'crispy_forms',
        'allauth',
        'sorl.thumbnail',
        'django_extensions',
        'django_inlinecss',
        'compressor',
        'djangobower',
        'dbbackup',
        'django_bootstrap_breadcrumbs',
        'huey.djhuey',
        'apps.core',
        # ...
        'apps.main',
    )

    TEMPLATE_CONTEXT_PROCESSORS = StandardConfiguration.TEMPLATE_CONTEXT_PROCESSORS + (
         "allauth.account.context_processors.account",
         "allauth.socialaccount.context_processors.socialaccount",
         'apps.main.context_processors.main',
    )

    AUTHENTICATION_BACKENDS = (
        # Needed to login by username in Django admin, regardless of `allauth`
        "django.contrib.auth.backends.ModelBackend",

        # `allauth` specific authentication methods, such as login by e-mail
        "allauth.account.auth_backends.AuthenticationBackend",
    )

    CRISPY_TEMPLATE_PACK = 'bootstrap3'

    STATICFILES_FINDERS = StandardConfiguration.STATICFILES_FINDERS + (
        'compressor.finders.CompressorFinder',
        'djangobower.finders.BowerFinder',
    )

    COMPRESS_PRECOMPILERS = (
        ('text/less', 'lessc {infile} {outfile}'),
    )

    LOGIN_URL = 'account_login'

    def BOWER_COMPONENTS_ROOT(self):
        return os.path.join(self.PROJECT_ROOT, 'components')

    # The way django-bower is used in this project is that we run
    # ./manage.py bower install locally, to add the packages to
    # components/bower_components.  However, this is under version control
    # so bower install doesn't need to be run by the other installations.
    BOWER_INSTALLED_APPS = (
    )

    @property
    def DEFAULT_DATABASE_ENGINE(self):
        # Location-based database
        return 'django.contrib.gis.db.backends.postgis'

