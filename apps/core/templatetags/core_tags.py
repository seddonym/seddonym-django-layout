from django import template
from django.conf import settings
from copy import copy
from crispy_forms.templatetags.crispy_forms_filters import flatatt_filter


register = template.Library()


@register.filter
def get_non_page_options(request):
    """Returns a urlencoded version of the GET options passed to the request,
    with the paginator page option removed.
    
    Used by templates/includes/paginator.html."""
    querydict = request.GET.copy()
    querydict.pop('page', None)
    return querydict.urlencode


@register.simple_tag
def base_url():
    """Returns the base url.
    
    Usage:
    
        {% templatetag openblock %} base_url {% templatetag closeblock %}
    """
    return settings.BASE_URL
